import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_picture_url(search_term):
    pexels_url = "https://api.pexels.com/v1/search?query=" + search_term

    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(pexels_url, headers=headers)

    response = json.loads(response.content)
    photos = response["photos"]
    
    return photos



def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct?q=" + city + "," + state + ",US&limit=5&appid=" + OPEN_WEATHER_API_KEY

    response = requests.get(url)
    data = json.loads(response.content)[0]
    lat = data["lat"]
    lon = data["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=" + OPEN_WEATHER_API_KEY

    response = requests.get(url)

    response = json.loads(response.content)
    weather = response["weather"]
    
    return {
        "weather": {
            "temp": weather["temp"],
            "description": weather["description"],
        }
    }

